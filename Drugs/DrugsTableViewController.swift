//
//  DrugsTableViewController.swift
//  Drugs
//
//  Created by Alfred Ayitey Ayi-Bonte on 4/6/16.
//  Copyright © 2016 Alfred Ayitey Ayi-Bonte. All rights reserved.
//

import UIKit

class DrugsTableViewController: UITableViewController, UISearchBarDelegate {

    @IBOutlet weak var searchLabel: UISearchBar!
    var drugs = [Drug]()
    var filtedDrugs = [Drug]()
    var drug : Drug?
    var searchActive :Bool  = false
    var networkhelper = NetworkHelper();
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpRefresh()
        searchLabel.delegate = self
        tableView.dataSource = self

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        print("result = \(searchText)")
        filtedDrugs = drugs.filter({ (drug) -> Bool in
            let tmp: NSString = drug.name
            let range = tmp.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch)
            return range.location != NSNotFound
        })
        if(filtedDrugs.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchActive = true
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchActive = false
    }
    
    
    
  
    
   
    @IBAction func refresh(sender: UIRefreshControl) {
        
    }
    func setUpRefresh(){
//        let refreshControl = UIRefreshControl()
//        refreshControl.addTarget(self, action: Selector("onRefresh:"), forControlEvents: UIControlEvents.ValueChanged)
    
        networkhelper.getDrugs({
            (drugs) in
            
            dispatch_async(dispatch_get_main_queue(), {
                //self.drugs.insert(drugs, atIndex: 0)
                if  let temp = drugs{
                    self.drugs = temp
                    self.tableView.reloadData()
                }
                
            })
        })
        
    }
    
    func onRefresh(){
        print("Hello world")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if searchActive {
        return filtedDrugs.count
        }
        return drugs.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("drugCell", forIndexPath: indexPath)
        var drug  = drugs[indexPath.row] as Drug
        
        if searchActive && ( (filtedDrugs.count - 1 ) > indexPath.row ){
        drug = filtedDrugs[indexPath.row] as Drug
        }
        
        cell.textLabel?.text = drug.name


        return cell
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
         drug = drugs[indexPath.row]
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using
         let controller = segue.destinationViewController as! DrugDetailViewController
        if let drugIndex = self.tableView.indexPathForSelectedRow{
        var drug = drugs[drugIndex.row]
            if searchActive && ( (filtedDrugs.count - 1 ) > drugIndex.row ) {
            drug = filtedDrugs[drugIndex.row]
            }
            
            controller.drug = drug
        }
        
        // Pass the selected object to the new view controller.
    }
    

}
