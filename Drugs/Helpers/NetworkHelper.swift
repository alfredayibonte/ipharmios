//
//  NetworkHelper.swift
//  Drugs
//
//  Created by Alfred Ayitey Ayi-Bonte on 4/6/16.
//  Copyright © 2016 Alfred Ayitey Ayi-Bonte. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class NetworkHelper{
    
    let URL = "http://ipharm.herokuapp.com/api/drugs/";
    
    //callback: ([Drug]?)->()
    func getDrugs(callback: ([Drug]?)->()) {
        Alamofire.request(.GET, "http://ipharm.herokuapp.com/api/drugs/")
            .responseJSON { response in
            
            var drugs = [Drug]()
            //starts
            if let value = response.result.value {
                let json = JSON(value)
                
                for i in 0  ..< json.count{
                    let drugJson = json[i]
                    let id = drugJson["id"].int!
                    let name = drugJson["name"].string!
                    let description = drugJson["description"].string!
                    
                    let temp = Drug(id: id, name: name, description: description)
                    drugs.append(temp)
                    
                   
                }
                callback(drugs)
                
                
            }
                //return drugs
            //end
            
        }
    }

}