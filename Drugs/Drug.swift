//
//  Drug.swift
//  Drugs
//
//  Created by Alfred Ayitey Ayi-Bonte on 4/6/16.
//  Copyright © 2016 Alfred Ayitey Ayi-Bonte. All rights reserved.
//

import Foundation
struct Drug {
    var id : Int
    var name : String
    var description : String
}