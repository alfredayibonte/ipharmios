//
//  DrugDetailViewController.swift
//  Drugs
//
//  Created by Alfred Ayitey Ayi-Bonte on 4/6/16.
//  Copyright © 2016 Alfred Ayitey Ayi-Bonte. All rights reserved.
//

import UIKit

class DrugDetailViewController: UIViewController {

   
    @IBOutlet weak var display: UITextView!
   
    var drug : Drug?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let temp = drug {
        display.text = temp.description
        self.navigationController?.navigationBar.topItem?.title = "Back"
        self.navigationItem.title = temp.name
        }
        
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
